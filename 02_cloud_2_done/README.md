# Домашнее задание к занятию "14.2 Синхронизация секретов с внешними сервисами. Vault"

## Задача 1: Работа с модулем Vault

Запустить модуль Vault конфигураций через утилиту kubectl в установленном minikube

0. Буду использовать инструмент werf

1. Подготовка к работе:

- Создал [репозиторий](https://gitlab.com/kezan86/netology/netology_cloud/-/tree/master/02_cloud_2)
- Создал файл проекта [werf.yaml](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/02_cloud_2/werf.yaml)
- Создал модуль vault - файл [vault.yaml](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/02_cloud_2/.helm/templates/vault.yaml)
- Модуль использую с контреллером `Deployment`
- Так-же в файл добавил создание `Service` для доступа по имени сервиса (а не по IP пода)
- Изменил скрипт питона (добавил функцию `print` для вывода информации в stdout, и добавил ипорт `os` для того чтобы использовать переменные окружения)
- Переменные окружения объявил в файле [_envs.tpl](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/02_cloud_2/.helm/templates/_envs.tpl)
- Файл скрипта [vault.py](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/02_cloud_2/vault.py)
- Файл скрипта импортируется в образ автоматически
- Создал `Job` для запуска `fedora` - файл [fedora.yaml](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/02_cloud_2/.helm/templates/fedora.yaml)

2. Запустил сборку образа

```
$ werf build
Version: v1.2.140+fix1
Using werf config render file: /private/var/folders/dt/h5ttnzf54pnf7tn07wwt9bym0000gn/T/werf-config-render-921994796

┌ Concurrent builds plan (no more than 5 images at the same time)
│ Set #0:
│ - ⛵ image app
│ - ⛵ image fedora
└ Concurrent builds plan (no more than 5 images at the same time)

┌ ⛵ image app
│ Use cache image for app/from
│      name: vault:f52368565ca945d04c86e8f83cfeda1d0d2881bc588b4dd5799e1c0b-1659537331369
│        id: 674d8b8a3a07
│   created: 2022-08-03 17:35:31.341273006 +0300 MSK
│      size: 192.7 MiB
└ ⛵ image app (0.02 seconds)

┌ ⛵ image fedora
│ Use cache image for fedora/from
│      name: vault:7bc7c7f7034d22ce4bc3abb2b685002cbdc16e190dac3289778977cd-1659537331279
│        id: eb26ba396596
│   created: 2022-08-03 17:35:31.253071548 +0300 MSK
│      size: 170.1 MiB
│
│ ┌ Building stage fedora/gitArchive
│ │ ┌ Store stage into :local
│ │ └ Store stage into :local (0.01 seconds)
│ ├ Info
│ │      name: vault:7604cd219480225d1fd7d9af4f9e0da8afc1a74e6ce57edd44566326-1659544180119
│ │        id: 2a3a2824d44a
│ │   created: 2022-08-03 19:29:40.062880677 +0300 MSK
│ │      size: 170.1 MiB (+5.3 KiB)
│ └ Building stage fedora/gitArchive (0.56 seconds)
│
│ ┌ Building stage fedora/dockerInstructions
│ │ ┌ Store stage into :local
│ │ └ Store stage into :local (0.01 seconds)
│ ├ Info
│ │      name: vault:98413169df174b86d259e30d3e1a843ce33fd7e4e1a7bdf5ce15810e-1659544180534
│ │        id: f03886eb7827
│ │   created: 2022-08-03 19:29:40.504996094 +0300 MSK
│ │      size: 170.1 MiB (+0 B)
│ └ Building stage fedora/dockerInstructions (0.39 seconds)
└ ⛵ image fedora (1.15 seconds)

Running time 1.37 seconds
```

3. Запустил деплой

```
$ werf converge --skip-build --repo=registry.gitlab.com/kezan86/container/vault --env=dev
Version: v1.2.140+fix1
Using werf config render file: /private/var/folders/dt/h5ttnzf54pnf7tn07wwt9bym0000gn/T/werf-config-render-2283585511

┌ Getting client id for the http synchronization server
│ Using clientID "b571b0bf-0b0f-4d7f-96ff-8f9abac30cfd" for http synchronization server at address https://synchronization.werf.io/b571b0bf-0b0f-4d7f-96ff-8f9abac30cfd
└ Getting client id for the http synchronization server (1.45 seconds)

┌ Concurrent builds plan (no more than 5 images at the same time)
│ Set #0:
│ - ⛵ image fedora
│ - ⛵ image app
└ Concurrent builds plan (no more than 5 images at the same time)

Use cache image for fedora/from
     name: registry.gitlab.com/kezan86/container/vault:7bc7c7f7034d22ce4bc3abb2b685002cbdc16e190dac3289778977cd-1659535812699
       id: f8b4f32e557d
  created: 2022-08-03 17:10:09 +0300 MSK
     size: 54.4 MiB

Use cache image for fedora/gitArchive
     name: registry.gitlab.com/kezan86/container/vault:7604cd219480225d1fd7d9af4f9e0da8afc1a74e6ce57edd44566326-1659544180119
       id: 2a3a2824d44a
  created: 2022-08-03 19:29:40 +0300 MSK
     size: 54.4 MiB (+2.7 KiB)

Use cache image for fedora/dockerInstructions
     name: registry.gitlab.com/kezan86/container/vault:98413169df174b86d259e30d3e1a843ce33fd7e4e1a7bdf5ce15810e-1659544180534
       id: f03886eb7827
  created: 2022-08-03 19:29:40 +0300 MSK
     size: 54.4 MiB (+0 B)

Use cache image for app/from
     name: registry.gitlab.com/kezan86/container/vault:f52368565ca945d04c86e8f83cfeda1d0d2881bc588b4dd5799e1c0b-1659532242781
       id: 72e2d3544bdc
  created: 2022-08-03 16:10:40 +0300 MSK
     size: 68.9 MiB

┌ Waiting for resources to become ready
│ ┌ Status progress
│ │ DEPLOYMENT                                                                                                         REPLICAS             AVAILABLE              UP-TO-DATE
│ │ vault                                                                                                              1/1                  1                      1
│ │ RESOURCE                                                                             NAMESPACE                  CONDITION: CURRENT (DESIRED)
│ │ Service/vault                                                                        dev                        -
│ └ Status progress
└ Waiting for resources to become ready (3.02 seconds)

┌ Waiting for resources elimination: jobs/vault
└ Waiting for resources elimination: jobs/vault (0.10 seconds)

┌ Waiting for helm hooks termination
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    5s                     0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    10s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    15s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    20s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    25s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    30s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ Fedora 36 - aarch64                             2.8 MB/s |  77 MB     00:27
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    35s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    40s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ Fedora 36 openh264 (From Cisco) - aarch64       1.8 kB/s | 2.5 kB     00:01
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    45s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ Fedora Modular 36 - aarch64                     1.0 MB/s | 2.3 MB     00:02
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    50s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    55s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ Fedora 36 - aarch64 - Updates                   2.0 MB/s |  23 MB     00:11
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    60s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ Fedora Modular 36 - aarch64 - Updates           1.4 MB/s | 2.3 MB     00:01
│ │ Dependencies resolved.
│ │ ================================================================================
│ │  Package                  Architecture Version               Repository    Size
│ │ ================================================================================
│ │ Installing:
│ │  python3-pip              noarch       21.3.1-2.fc36         fedora       1.8 M
│ │ Installing weak dependencies:
│ │  python3-setuptools       noarch       59.6.0-2.fc36         fedora       936 k
│ │
│ │ Transaction Summary
│ │ ================================================================================
│ │ Install  2 Packages
│ │
│ │ Total download size: 2.7 M
│ │ Installed size: 14 M
│ │ Downloading Packages:
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    65s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ (1/2): python3-setuptools-59.6.0-2.fc36.noarch. 546 kB/s | 936 kB     00:01
│ │ (2/2): python3-pip-21.3.1-2.fc36.noarch.rpm     701 kB/s | 1.8 MB     00:02
│ │ --------------------------------------------------------------------------------
│ │ Total                                           805 kB/s | 2.7 MB     00:03
│ │ Running transaction check
│ │ Transaction check succeeded.
│ │ Running transaction test
│ │ Transaction test succeeded.
│ │ Running transaction
│ │   Preparing        :                                                        1/1
│ │   Installing       : python3-setuptools-59.6.0-2.fc36.noarch                1/2
│ │   Installing       : python3-pip-21.3.1-2.fc36.noarch                       2/2
│ │   Running scriptlet: python3-pip-21.3.1-2.fc36.noarch                       2/2
│ │   Verifying        : python3-pip-21.3.1-2.fc36.noarch                       1/2
│ │   Verifying        : python3-setuptools-59.6.0-2.fc36.noarch                2/2
│ │
│ │ Installed:
│ │   python3-pip-21.3.1-2.fc36.noarch    python3-setuptools-59.6.0-2.fc36.noarch
│ │
│ │ Complete!
│ │ Collecting hvac
│ │   Downloading hvac-0.11.2-py2.py3-none-any.whl (148 kB)
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    70s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       1/1             0                     Running                      Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ job/vault po/vault-k29pz container/init logs
│ │ Collecting six>=1.5.0
│ │   Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
│ │ Collecting requests>=2.21.0
│ │   Downloading requests-2.28.1-py3-none-any.whl (62 kB)
│ │ Collecting certifi>=2017.4.17
│ │   Downloading certifi-2022.6.15-py3-none-any.whl (160 kB)
│ │ Collecting urllib3<1.27,>=1.21.1
│ │   Downloading urllib3-1.26.11-py2.py3-none-any.whl (139 kB)
│ │ Collecting idna<4,>=2.5
│ │   Downloading idna-3.3-py3-none-any.whl (61 kB)
│ │ Collecting charset-normalizer<3,>=2
│ │   Downloading charset_normalizer-2.1.0-py3-none-any.whl (39 kB)
│ │ Installing collected packages: urllib3, idna, charset-normalizer, certifi, six, requests, hvac
│ │ WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead:            ↵
│ │ https://pip.pypa.io/warnings/venv
│ │ Successfully installed certifi-2022.6.15 charset-normalizer-2.1.0 hvac-0.11.2 idna-3.3 requests-2.28.1 six-1.16.0 urllib3-1.26.11
│ │ True
│ │ {'request_id': '81947098-a6df-f204-ff66-6c0f53dc6fc9', 'lease_id': '', 'renewable': False, 'lease_duration': 0, 'data': {'created_time': '2022-08-03T16:33:20.194174917Z', 'custom_metadata': None,    ↵
│ │ 'deletion_time': '', 'destroyed': False, 'version': 1}, 'wrap_info': None, 'warnings': None, 'auth': None}
│ │ {'request_id': 'fc062ea0-98ff-65d8-bb66-9d70ba9f54a5', 'lease_id': '', 'renewable': False, 'lease_duration': 0, 'data': {'data': {'netology': 'Big secret!!!'}, 'metadata': {'created_time':           ↵
│ │ '2022-08-03T16:33:20.194174917Z', 'custom_metadata': None, 'deletion_time': '', 'destroyed': False, 'version': 1}}, 'wrap_info': None, 'warnings': None, 'auth': None}
│ └ job/vault po/vault-k29pz container/init logs
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              1                    75s                    0/0
│ │ │   POD                                         READY           RESTARTS              STATUS                       ---
│ │ └── k29pz                                       0/1             0                     Running->Completed           Waiting for: pods should be terminated, succeeded 0->1
│ └ Status progress
│
│ ┌ Status progress
│ │ JOB                                                                                                                ACTIVE               DURATION               SUCCEEDED/FAILED
│ │ vault                                                                                                              0                    77s                    0->1/0
│ │ │   POD                                         READY           RESTARTS              STATUS
│ │ └── k29pz                                       0/1             0                     Completed
│ └ Status progress
└ Waiting for helm hooks termination (76.51 seconds)

Release "vault-dev" has been upgraded. Happy Helming!
NAME: vault-dev
LAST DEPLOYED: Wed Aug  3 19:32:04 2022
LAST PHASE: hooks-post
LAST STAGE: 0
NAMESPACE: dev
STATUS: deployed
REVISION: 2
TEST SUITE: None
Running time 82.89 seconds
```

4. Получить значение внутреннего IP пода

```
kubectl -n dev get po -o wide
NAME                    READY   STATUS      RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
vault-dd6ccbc46-2bh7x   1/1     Running     0          2m59s   172.17.0.8   minikube   <none>           <none>
vault-k29pz             0/1     Completed   0          2m6s    172.17.0.9   minikube   <none>           <none>
```

- IP-адрес мне не нужне так как приложение будет идти по имени сервиса

```
kubectl -n dev get svc
NAME    TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
vault   ClusterIP   None         <none>        8200/TCP   4m12s
```

5. Проверка работы джобы с `fedora`

- Установить дополнительные пакеты (Устанавливаются джобой)
- Запустить интепретатор Python и выполнить следующий код, предварительно поменяв IP и токен (Интерпретатор Python запускается джобой, и IP и токен задаются переменными окружениями)

- Лог работы Джобы

```
kubectl -n dev get logs vault-k29pz -f
error: flag needs an argument: 'f' in -f
See 'kubectl get --help' for usage.
(base) ➜  ~ kubectl -n dev logs vault-k29pz -f
Fedora 36 - aarch64                             2.8 MB/s |  77 MB     00:27
Fedora 36 openh264 (From Cisco) - aarch64       1.8 kB/s | 2.5 kB     00:01
Fedora Modular 36 - aarch64                     1.0 MB/s | 2.3 MB     00:02
Fedora 36 - aarch64 - Updates                   2.0 MB/s |  23 MB     00:11
Fedora Modular 36 - aarch64 - Updates           1.4 MB/s | 2.3 MB     00:01
Dependencies resolved.
================================================================================
 Package                  Architecture Version               Repository    Size
================================================================================
Installing:
 python3-pip              noarch       21.3.1-2.fc36         fedora       1.8 M
Installing weak dependencies:
 python3-setuptools       noarch       59.6.0-2.fc36         fedora       936 k

Transaction Summary
================================================================================
Install  2 Packages

Total download size: 2.7 M
Installed size: 14 M
Downloading Packages:
(1/2): python3-setuptools-59.6.0-2.fc36.noarch. 546 kB/s | 936 kB     00:01
(2/2): python3-pip-21.3.1-2.fc36.noarch.rpm     701 kB/s | 1.8 MB     00:02
--------------------------------------------------------------------------------
Total                                           805 kB/s | 2.7 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : python3-setuptools-59.6.0-2.fc36.noarch                1/2
  Installing       : python3-pip-21.3.1-2.fc36.noarch                       2/2
  Running scriptlet: python3-pip-21.3.1-2.fc36.noarch                       2/2
  Verifying        : python3-pip-21.3.1-2.fc36.noarch                       1/2
  Verifying        : python3-setuptools-59.6.0-2.fc36.noarch                2/2

Installed:
  python3-pip-21.3.1-2.fc36.noarch    python3-setuptools-59.6.0-2.fc36.noarch

Complete!
Collecting hvac
  Downloading hvac-0.11.2-py2.py3-none-any.whl (148 kB)
Collecting six>=1.5.0
  Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
Collecting requests>=2.21.0
  Downloading requests-2.28.1-py3-none-any.whl (62 kB)
Collecting certifi>=2017.4.17
  Downloading certifi-2022.6.15-py3-none-any.whl (160 kB)
Collecting urllib3<1.27,>=1.21.1
  Downloading urllib3-1.26.11-py2.py3-none-any.whl (139 kB)
Collecting idna<4,>=2.5
  Downloading idna-3.3-py3-none-any.whl (61 kB)
Collecting charset-normalizer<3,>=2
  Downloading charset_normalizer-2.1.0-py3-none-any.whl (39 kB)
Installing collected packages: urllib3, idna, charset-normalizer, certifi, six, requests, hvac
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
Successfully installed certifi-2022.6.15 charset-normalizer-2.1.0 hvac-0.11.2 idna-3.3 requests-2.28.1 six-1.16.0 urllib3-1.26.11
True
{'request_id': '81947098-a6df-f204-ff66-6c0f53dc6fc9', 'lease_id': '', 'renewable': False, 'lease_duration': 0, 'data': {'created_time': '2022-08-03T16:33:20.194174917Z', 'custom_metadata': None, 'deletion_time': '', 'destroyed': False, 'version': 1}, 'wrap_info': None, 'warnings': None, 'auth': None}
{'request_id': 'fc062ea0-98ff-65d8-bb66-9d70ba9f54a5', 'lease_id': '', 'renewable': False, 'lease_duration': 0, 'data': {'data': {'netology': 'Big secret!!!'}, 'metadata': {'created_time': '2022-08-03T16:33:20.194174917Z', 'custom_metadata': None, 'deletion_time': '', 'destroyed': False, 'version': 1}}, 'wrap_info': None, 'warnings': None, 'auth': None}
```

- Как видно по выаоду, скрипт отработал успешно
- Скриншот

<img src="./img/1.png" alt="">