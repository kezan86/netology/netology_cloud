{{- define "app_env" }}
- name: VAULT_DEV_ROOT_TOKEN_ID
  value: {{ pluck .Values.werf.env .Values.app.vault.id | first | default .Values.app.vault.id._default | quote }}
- name: VAULT_DEV_LISTEN_ADDRESS
  value: {{ pluck .Values.werf.env .Values.app.vault.address | first | default .Values.app.vault.address._default | quote }}
- name: VAULT_SERVICE
  value: http://{{ .Chart.Name }}:8200
{{- end }}
