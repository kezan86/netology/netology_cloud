import hvac
import os
client = hvac.Client(
    url=os.environ.get('VAULT_SERVICE'),
    token=os.environ.get('VAULT_DEV_ROOT_TOKEN_ID')
)
print(client.is_authenticated())

# Пишем секрет
print(client.secrets.kv.v2.create_or_update_secret(
    path='hvac',
    secret=dict(netology='Big secret!!!'),
))

# Читаем секрет
print(client.secrets.kv.v2.read_secret_version(
    path='hvac',
))
