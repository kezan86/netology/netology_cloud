# Домашнее задание к занятию "14.3 Карты конфигураций"

## Задача 1: Работа с картами конфигураций через утилиту kubectl в установленном minikube

Выполните приведённые команды в консоли. Получите вывод команд. Сохраните
задачу 1 как справочный материал.

### Как создать карту конфигураций?

```
kubectl create configmap nginx-config --from-file=nginx.conf
kubectl create configmap domain --from-literal=name=netology.ru
```

### Ответ:

```
✗ kubectl -n dev create cm nginx-config --from-file=nginx.conf
configmap/nginx-config created

✗ kubectl -n dev create cm domain --from-literal=name=netology.ru
configmap/domain created
```

### Как просмотреть список карт конфигураций?

```
kubectl get configmaps
kubectl get configmap
```

### Ответ:

```
✗ kubectl -n dev get cm
NAME                   DATA   AGE
domain                 1      20s
kube-root-ca.crt       1      2d23h
nginx-config           1      30s
werf-synchronization   0      2d23h
```

### Как просмотреть карту конфигурации?

```
kubectl get configmap nginx-config
kubectl describe configmap domain
```

### Ответ:

```
✗ kubectl -n dev get cm nginx-config
NAME           DATA   AGE
nginx-config   1      2m29s

✗ kubectl -n dev describe cm domain
Name:         domain
Namespace:    dev
Labels:       <none>
Annotations:  <none>

Data
====
name:
----
netology.ru

BinaryData
====

Events:  <none>
```

### Как получить информацию в формате YAML и/или JSON?

```
kubectl get configmap nginx-config -o yaml
kubectl get configmap domain -o json
```

### Ответ:

```
✗ kubectl -n dev get cm nginx-config -o yaml
apiVersion: v1
data:
  nginx.conf: |
    server {
        listen 80;
        server_name  netology.ru www.netology.ru;
        access_log  /var/log/nginx/domains/netology.ru-access.log  main;
        error_log   /var/log/nginx/domains/netology.ru-error.log info;
        location / {
            include proxy_params;
            proxy_pass http://10.10.10.10:8080/;
        }
    }
kind: ConfigMap
metadata:
  creationTimestamp: "2022-08-05T07:42:02Z"
  name: nginx-config
  namespace: dev
  resourceVersion: "213414"
  uid: 49818bf9-39d0-46d7-8774-59b3f2d5801e

✗ kubectl -n dev get cm domain -o json
{
    "apiVersion": "v1",
    "data": {
        "name": "netology.ru"
    },
    "kind": "ConfigMap",
    "metadata": {
        "creationTimestamp": "2022-08-05T07:42:12Z",
        "name": "domain",
        "namespace": "dev",
        "resourceVersion": "213423",
        "uid": "73edd931-d140-4884-9361-48888bfd5c35"
    }
}
```

### Как выгрузить карту конфигурации и сохранить его в файл?

```
kubectl get configmaps -o json > configmaps.json
kubectl get configmap nginx-config -o yaml > nginx-config.yml
```

### Ответ:

```
✗ kubectl -n dev get cm -o json > configmaps.json

✗ kubectl -n dev get cm nginx-config -o yaml > nginx-config.yml
```

### Как удалить карту конфигурации?

```
kubectl delete configmap nginx-config
```

### Ответ:

```
✗ kubectl -n dev delete cm nginx-config
configmap "nginx-config" deleted
```

### Как загрузить карту конфигурации из файла?

```
kubectl apply -f nginx-config.yml
```

### Ответ:

```
✗ kubectl -n dev apply -f nginx-config.yml
configmap/nginx-config created
```

## Задача 2 (*): Работа с картами конфигураций внутри модуля

Выбрать любимый образ контейнера, подключить карты конфигураций и проверить
их доступность как в виде переменных окружения, так и в виде примонтированного
тома


## Ответ:

1. Создание `ConfigMap` определено в файле [configmap.yaml](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/03_cloud/.helm/templates/configmap.yaml)

```
✗ kubectl -n dev apply -f .helm/templates/configmap.yaml
configmap/nginx-config created

✗ kubectl -n dev get cm
NAME                   DATA   AGE
kube-root-ca.crt       1      3d2h
nginx-config           2      12s
werf-synchronization   0      3d2h

✗ kubectl -n dev get cm nginx-config -o yaml
apiVersion: v1
data:
  index.html: |
    <H1> Cloud 3 </H1>
  nginx.conf: |
    server {
        listen 80;
        server_name  kezan86.ru www.kezan86.ru;
        access_log  /dev/stdout;
        error_log   /dev/stderr;
        location / {
            root   /config;
            index  index.html;
        }
    }
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","data":{"index.html":"\u003cH1\u003e Cloud 3 \u003c/H1\u003e\n","nginx.conf":"server {\n    listen 80;\n    server_name  kezan86.ru www.kezan86.ru;\n    access_log  /dev/stdout;\n    error_log   /dev/stderr;\n    location / {\n        root   /config;\n        index  index.html;\n    }\n}\n"},"kind":"ConfigMap","metadata":{"annotations":{},"labels":{"app":"nginx"},"name":"nginx-config","namespace":"dev"}}
  creationTimestamp: "2022-08-05T10:49:11Z"
  labels:
    app: nginx
  name: nginx-config
  namespace: dev
  resourceVersion: "224181"
  uid: 40bd0ccd-a333-4464-832b-78d782e04a1b
```

2. Запуск пода определен в файле [deployment.yaml](https://gitlab.com/kezan86/netology/netology_cloud/-/blob/master/03_cloud/.helm/templates/deployment.yaml)

```
✗ kubectl -n dev apply -f .helm/templates/deployment.yaml
deployment.apps/nginx created
service/nginx created

✗ kubectl -n dev get deploy
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   1/1     1            1           20s

✗ kubectl -n dev get po
NAME                     READY   STATUS    RESTARTS   AGE
nginx-74b7874c67-qpbcr   1/1     Running   0          27s

✗ kubectl -n dev exec -ti nginx-74b7874c67-qpbcr bash
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl exec [POD] -- [COMMAND] instead.
root@nginx-74b7874c67-qpbcr:/#


root@nginx-74b7874c67-qpbcr:/# ls -l /config/
total 0
lrwxrwxrwx 1 root root 17 Aug  5 10:51 index.html -> ..data/index.html
root@nginx-74b7874c67-qpbcr:/#
root@nginx-74b7874c67-qpbcr:/#
root@nginx-74b7874c67-qpbcr:/# cat /config/index.html
<H1> Cloud 3 </H1>
root@nginx-74b7874c67-qpbcr:/#

root@nginx-74b7874c67-qpbcr:/# ls -l /etc/nginx/conf.d/
total 0
lrwxrwxrwx 1 root root 17 Aug  5 10:51 nginx.conf -> ..data/nginx.conf
root@nginx-74b7874c67-qpbcr:/#
root@nginx-74b7874c67-qpbcr:/#
root@nginx-74b7874c67-qpbcr:/# cat  /etc/nginx/conf.d/nginx.conf
server {
    listen 80;
    server_name  kezan86.ru www.kezan86.ru;
    access_log  /dev/stdout;
    error_log   /dev/stderr;
    location / {
        root   /config;
        index  index.html;
    }
}
root@nginx-74b7874c67-qpbcr:/#

root@nginx-74b7874c67-qpbcr:/# echo ${INDEX}
<H1> Cloud 3 </H1>
root@nginx-74b7874c67-qpbcr:/#

root@nginx-74b7874c67-qpbcr:/# echo ${NGINX_CONFIG}
server { listen 80; server_name kezan86.ru www.kezan86.ru; access_log /dev/stdout; error_log /dev/stderr; location / { root /config; index index.html; } }
root@nginx-74b7874c67-qpbcr:/#
```

- Скрин работы `Nginx`

<img src="./img/1.png" alt="">